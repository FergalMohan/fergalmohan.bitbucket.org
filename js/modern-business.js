// Activates the Carousel
$('.carousel').carousel({
  interval: 5000
});

// Activates Tooltips for Social Links
$('.tooltip-social').tooltip({
  selector: "a[data-toggle=tooltip]"
});

// Read a page's GET URL variables and return them as an associative array.
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

        var portfolioJSONData = '{"items":[{"title":"WordGap", "description":"WordGap is an innovative eLearning Mobile App to help students with vocabulary", "image":"img/wordgapipad.jpg"},{"title":"KundaliniYoga.ie", "description":"KundaliniYoga.ie is the home of the Kundalini Yoga Teachers Association of Ireland", "image":"img/kundaliniyoga_ie.jpg"},{"title":"Trainedin.com", "description":"Trainedin is a company specializing in eLearning", "image":"img/trainedinlmspage.jpg"},{"title":"WellWithin.ie", "description":"WellWithin.ie is the online presemce of a Holistic Wellness Centre", "image":"img/wellwithinpage.jpg"},{"title":"Yogapp", "description":"Yogapp is a native Mobile App that gives Yoga students a way to manage and playback their own personal practices.", "image":"img/yogappcompositegrabs.jpg"},{"title":"StepGap.com", "description":"StepGap.com specializes in licensing and consulting on Mobile  Technology", "image":"img/stepgappage.jpg"},{"title":"ScienceDirect.ie", "description":"Science Direct Ltd is an analytical and ecological consultancy company.", "image":"img/sciencedirect_ie.jpg"},{"title":"Top Secret", "description":"We have been involved in a number of sensitive projects which we can discuss in general terms.", "image":"img/topsecretstamp_sm.jpg"}]}';
        
        var jsonDetails = {"WordGap":['Native Mobile App','Backbone Web Application','Content Management System','Content Sharing'],"KundaliniYoga.ie":['Public Facing Website','Content Management System','Authenticated Access for Members','Content Creation e,g. Blogs, Events, Articles'],"Trainedin.com":['Corporate Website', 'Learning Management System', 'Mobile Apps', 'Content Development'],"WellWithin.ie":['Company Website', 'Contact Management System', 'Graphic Design', 'Content Development'],"Yogapp":['Native Mobile App', 'In App Purchase', 'Push Notifications', 'Content Development'],"StepGap.com":['Corporate Website', 'Content Management System', 'Theme Design', 'Content Development'],"ScienceDirect.ie":['Company Website', 'Contact Management System', 'Graphic Design', 'Content Development'],"Top Secret":['Native Mobile Apps','Websites','Web Apps','Content Development']};

    function showDetailsForSelection(pageFromParams)
    {
        var portfolioArray = JSON.parse(portfolioJSONData);
        var imagePath = portfolioArray.items[pageFromParams]["image"];
        var title = portfolioArray.items[pageFromParams]["title"];
        var description = portfolioArray.items[pageFromParams]["description"];
        $("#main-image-placeholder").attr("src",imagePath);
        $("#main-title-placeholder span").text(title);
        $("#main-description-placeholder span").text(description);
        //$("#main-details-placeholder").replaceWith("");     /*empty it*/
        var detailsItems = jsonDetails[title];
        $("#main-details-placeholder").html("");
        $(detailsItems).each(function(index, item) {
            var listElem = $(document.createElement('li')).text(item);
            $("#main-details-placeholder").append(listElem);
        });
    }